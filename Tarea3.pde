import ddf.minim.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import ddf.minim.signals.*;
import ddf.minim.spi.*;
import ddf.minim.ugens.*;
 
Minim minim;
AudioPlayer player;
int tiempo1=0;
int tiempo2=0;
int temperature=0;
int pressure=0;

void setup(){
size(1450,850);
minim=new Minim(this);
player=minim.loadFile("SD_ALERT_29.mp3");
}

void draw(){ 
background(0,255,255);
fill(255);
ellipse(350,300,300,300);
ellipse(950,300,300,300);

textSize(50);text("Sensor",555,50);
textSize(40);text("pressure",280,475);
textSize(40);text("temperature",850,475);
textSize(25);text(pressure,343,500);
textSize(25);text(temperature,945,500);

float distancia1=dist(mouseX,mouseY,350,300);
float distancia2=dist(mouseX,mouseY,950,300);

if (distancia1<150){
pressure=pressure+1;
fill(255,255-(tiempo1*0.5),255-(tiempo1*0.5));
ellipse(350,300,300,300);
textSize(35);text("Tiempo (s)",580,270);
tiempo1=tiempo1+1;
textSize(35);text(tiempo1/100,634,305);
if ((tiempo1/100)>2){
player.play();
textSize(50);text("¡¡ALERTA!!",555,350);
textSize(25);text("¡tiempo superado (5 seg)!",520,400);
}
else{
player.rewind();
}}
else{
pressure=pressure-1;
tiempo1=0;
if (pressure<0){
pressure=0; 
}}


if (distancia2<150){
temperature=temperature+1;
fill(255,255-(tiempo2*0.5),255-(tiempo2*0.5));
ellipse(950,300,300,300);
textSize(35);text("Tiempo(s)",580,270);
tiempo2=tiempo2+1;
textSize(35);text(tiempo2/100,634,305);
if ((tiempo2/100)>2){
player.play();
textSize(50);text("¡ALERTA!",555,350);
textSize(25);text("¡tiempo superado (3 seg)!",520,400);
}
else{
player.rewind();
}}
else{
temperature=temperature-1;
tiempo2=0;
if (temperature<0){
temperature=0; 
}}}
